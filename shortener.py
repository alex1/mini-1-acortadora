from webapp import webApp


class Shortener(webApp):
    def __init__(self, hostname, port):
        self.memory = {}  # Diccionario que dado short -> url
        super().__init__(hostname, port)

    def _render_html(self):
        with open('formulario.html') as form:
            html_text = form.read()

            shortened_urls = "<ul>"
            for short, url in self.memory.items():
                if 'http://' not in url and 'https://' not in url:
                    url = 'https://' + url
                shortened_urls += f'<li>{short}: <a href="{url}">{url}</a></li>'
            shortened_urls += "</ul>"

            return html_text.format(shortened_urls)

    def parse(self, request):
        lines = request.split('\n')
        method, path, http = lines[0].split()
        result = {
            'path': path,
            'method': method
        }

        if method == 'POST':
            result['context'] = {}
            content_text = lines[-1].split('&')
            for content_line in content_text:
                field, value = content_line.split('=')
                result['context'][field] = value

        return result

    def process(self, parsedRequest):
        path = parsedRequest['path']
        method = parsedRequest['method']

        if path == '/':
            if method == 'GET':
                return "200 OK", self._render_html()

            elif method == 'POST':
                url = parsedRequest['context']['url']
                url = url.replace('%3A', ':')
                url = url.replace('%2F', '/')
                short = parsedRequest['context']['short']
                self.memory[short] = url
                return "200 OK", self._render_html()

        return "404 NOT FOUND", "<html><body>Cannot find requested content</body></html>"


if __name__ == "__main__":
    shortener = Shortener("localhost", 1234)
